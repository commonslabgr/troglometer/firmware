/* Troglo.ino
 * 
 * Firmware for a data-logging environment monitor "Troglometer".
 * 
 * Copyright (c) 2020 by Jann Eike KRUSE and CommonsLab Koin.S.Ep.
 * 
 * This program is Free Software, since it is 
 * licensed under the GNU AGPL 3.0 licence. 
 * 
 */

//#define DISABLE_AUTO_OFF

#define AIR_TYPE 1
#define H2O_TYPE 2
#define CAM_TYPE 3

#define DEVICE_TYPE AIR_TYPE  // AIR, H2O, or CAM type?

/********************
 * GLOBAL CONSTANTS *
 ********************/
 
 
const char VERSION_STRING[] = "v3.1-Cal";
const char DEVICE_UUID[]    = "26e084d0-aeb5-4403-8c39-75ba450eebaf";

#if DEVICE_TYPE == AIR_TYPE
const char DEVICE_NAME[]    = "AIR";
#endif

#if DEVICE_TYPE == H2O_TYPE
const char DEVICE_NAME[]    = "H2O";
#endif

#if DEVICE_TYPE == CAM_TYPE
const char DEVICE_NAME[]    = "CAM";
#endif


enum AnalogPin {
  PIN_ADC      = A0,
  PIN_WIND_DIR = A1,
//PIN_A2       = A2,
//PIN_A3       = A3,
  PIN_SDA      = A4,
  PIN_SCL      = A5,
  PIN_BATT25   = A6,
  PIN_BATT50   = A7,
};

enum DigitalPin {
  PIN_RX          =  0, // Serial UART receive data  (from user to ARDUINO)
  PIN_TX          =  1, // Serial UART transmit data (from ARDUINO to user)
  PIN_WIND_SPEED  =  2, // Interrupt'able pin for anemometer clicks.
  PIN_SQW         =  3, // Interrupt'able pin for clock alarm (or "square wave")
  PIN_DS          =  4, // Dallas One-Wire bus
  PIN_LED         =  5, // Status LED
  PIN_PWR_FLASH   =  6, // Power up the IR flash light
  PIN_PWR_PERI    =  7, // Power up the peripherals (sensors, SD reader, clock, etc.)
  PIN_PWR_MAIN    =  8, // Power up main power bus (flash, peripherals and ARDUINO power)
  PIN_CS_SD       =  9, // SPI: Chip-select signal for the SD-card reader
  PIN_CS_CAM      = 10, // SPI: Chip-select signal for the ArduCAM
  PIN_MOSI        = 11, // SPI: Master-Out--Slave-In
  PIN_MISO        = 12, // SPI: Master-In--Slave-Out
  PIN_SCK         = 13, // SPI: Serial Clock
};

enum EepromAddress {             // Addresses to store user preferences:
  EEPROM_ADDR_SLEEP_MINUTES = 0, // How many minutes to sleep between measurements
  EEPROM_ADDR_H2O_COND_ZERO = 1, // Zero-point ADC value    - offset (What does the ADC give at 0.00 us/cm?)
  EEPROM_ADDR_H2O_COND_SLOPE= 2, // us/cm per 10 ADC digits - slope  (What is the correct us/cm for an ADC of 10?)
  EEPROM_ADDR_GND_COND_ZERO = 3, // Zero-point ADC value    - offset (What does the ADC give at 0%, i.e. dry air?)
  EEPROM_ADDR_GND_COND_SLOPE= 4, // us/cm per 10 ADC digits - slope  (What is the correct moistrue % for an ADC of 10?)
};

// Store preferences
//#include <EEPROM.h>

// POWER
  #include "LowPower.h"
  #include <avr/power.h>
  uint8_t sleepMinutes = 1; // Number of minutes to stay asleep.

// TIME
  #include <Wire.h>
  #include "DS3231.h"
  DS3231 Clock;
  RTClib RTC;
  DateTime currentDateTime;

// SD-card:
  #include <SPI.h>
  #include <SD.h>
  Sd2Card card;
  SdVolume volume;
  SdFile root;
  File dataFile;
  //boolean SDcardOn = true;     
  byte  keep_SPCR;
  String logString = "";

#if DEVICE_TYPE == H2O_TYPE
  int16_t h2oCondZero  = 0;
  int16_t h2oCondSlope = -1;
#endif

#if DEVICE_TYPE == AIR_TYPE
  int16_t airCapZero  = 0;
  int16_t airCapSlope = -1;
#endif

#if DEVICE_TYPE == AIR_TYPE
// TSL2591 Light Sensor
  #include <Wire.h>
  #include <Adafruit_Sensor.h>
  #include "Adafruit_TSL2591.h"
  Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591); // pass in a number for the sensor identifier (for your use later)
#endif

#if (DEVICE_TYPE == AIR_TYPE || DEVICE_TYPE == H2O_TYPE)
// DS18B20 Waterproof temperature sensor
  #include <OneWire.h>
  #include <DallasTemperature.h>
  OneWire oneWire(PIN_DS);
  DallasTemperature sensors(&oneWire);
#endif

#if DEVICE_TYPE == AIR_TYPE
// Wind
  volatile uint32_t windCounter = 0;
  unsigned long windStartTime = 0;
#endif

#if DEVICE_TYPE == AIR_TYPE
// BME280 Humidity, Temperature, Pressure
  #include <Wire.h>
  #include <BMx280I2C.h>
  BMx280I2C bme280int(0x77); // Alternative address (E.g. GYBMP280-3.3 6-pin board with SDO high)
  BMx280I2C bme280ext(0x76); // Standard address (E.g. GYBMEP 4-pin board onmodified)
#endif

#if DEVICE_TYPE == CAM_TYPE
// ArduCam
#include <Wire.h>
#include <ArduCAM.h>
#include <SPI.h>
#include <SD.h>
#include "memorysaver.h"
#define   FRAMES_NUM    0x00
  ArduCAM myCAM1(OV5642, PIN_CS_CAM);
#endif

boolean battLow = false;

void setup() {
  initPower();
  initLED();
  initSerial();
  initI2C();
  initSD();

  initPrefs();

  logReboot();

#if DEVICE_TYPE == CAM_TYPE
  initCamera();
#endif

#if (DEVICE_TYPE == AIR_TYPE || DEVICE_TYPE == H2O_TYPE)
  initDS18B20();
#endif

#if DEVICE_TYPE == AIR_TYPE
  initWindSpeed();
  initTSL2591();
#endif

#if DEVICE_TYPE == AIR_TYPE
  initBME280();
#endif
  
  SDreadInterval();

#if DEVICE_TYPE == H2O_TYPE  
  SDreadH2oOffset();
  SDreadH2oZero();
#endif
  
#if DEVICE_TYPE == AIR_TYPE  
  SDreadAirOffset();
  SDreadAirZero();
#endif

#if (DEVICE_TYPE == AIR_TYPE || DEVICE_TYPE == H2O_TYPE)
  bootMessage();
#endif

  logHeader();
}

void SDreadInterval() {
  char str[6];
  uint8_t stringPointer = 0;
  File dataFile = SD.open("interval.txt");

  if (dataFile) {
    while ((dataFile.available() && stringPointer < 5)) {
      str[stringPointer]=dataFile.read();
      stringPointer++;
    }
    dataFile.close();
    sleepMinutes = atol(str);
    //EEPROM.update(EEPROM_ADDR_SLEEP_MINUTES, sleepMinutes);
  }
}

#if DEVICE_TYPE == H2O_TYPE  
void SDreadH2oOffset() {
  char str[6];
  uint8_t stringPointer = 0;
  File dataFile = SD.open("H2Ozero.txt");

  if (dataFile) {
    while ((dataFile.available() && stringPointer < 5)) {
      str[stringPointer]=dataFile.read();
      stringPointer++;
    }
    dataFile.close();
    h2oCondZero = atol(str);
    //EEPROM.update(, h2oCondZero);
  }
}
#endif

#if DEVICE_TYPE == AIR_TYPE  
void SDreadAirOffset() {
  char str[6];
  uint8_t stringPointer = 0;
  File dataFile = SD.open("AIRzero.txt");

  if (dataFile) {
    while ((dataFile.available() && stringPointer < 5)) {
      str[stringPointer]=dataFile.read();
      stringPointer++;
    }
    dataFile.close();
    airCapZero = atol(str);
    //EEPROM.update(, airCondZero);
  }
}
#endif


#if DEVICE_TYPE == H2O_TYPE  
void SDreadH2oZero() {
  char str[6];
  uint8_t stringPointer = 0;
  File dataFile = SD.open("H2Oslope.txt");

  if (dataFile) {
    while ((dataFile.available() && stringPointer < 5)) {
      str[stringPointer]=dataFile.read();
      stringPointer++;
    }
    dataFile.close();
    h2oCondSlope = atol(str);
    //EEPROM.update(, h2oCondSlope);
  }
}
#endif

#if DEVICE_TYPE == AIR_TYPE  
void SDreadAirZero() {
  char str[6];
  uint8_t stringPointer = 0;
  File dataFile = SD.open("AIRslope.txt");

  if (dataFile) {
    while ((dataFile.available() && stringPointer < 5)) {
      str[stringPointer]=dataFile.read();
      stringPointer++;
    }
    dataFile.close();
    airCapSlope = atol(str);
    //EEPROM.update(, AirCondSlope);
  }
}
#endif


void loop() {
  logMeasurements();
  shutDown();
  sleep();
  wakeUp();
}

void initPrefs(){
    //sleepMinutes = EEPROM.read(EEPROM_ADDR_SLEEP_MINUTES);
}

void bootMessage() {
  while (Serial.available() > 0) Serial.read(); // discard previous data
  //Serial.println( F("Press [ENTER].") );
  for (uint8_t counter = 10; counter >= 1; counter--) {
    Serial.print(counter);
    Serial.print( F(".") );
    digitalWrite(PIN_LED, HIGH); 
    delay(100);
    digitalWrite(PIN_LED, LOW); 
    delay(900);
    while (Serial.available() > 0) {
      int character = Serial.read();
      switch (character) {
        case 10:
        case 13: 
          counter = 1;
          bootMenu(); 
        break;
          
        default: 
          Serial.write(character); 
          Serial.println("??");
        break;
      }
    }
  }
  Serial.println();
}

void bootMenu() {
  while (Serial.available() > 0) Serial.read(); // discard previous data
  Serial.println( F("\nt: Set time\nq: Quit menu") );
  for (uint8_t counter = 30; counter >= 1; counter--) {
    Serial.print(counter);
    Serial.print( F("..") );
    digitalWrite(PIN_LED, HIGH); 
    delay(50);
    digitalWrite(PIN_LED, LOW); 
    delay(100);
    digitalWrite(PIN_LED, HIGH); 
    delay(50);
    digitalWrite(PIN_LED, LOW); 
    delay(800);
    while (Serial.available() > 0) {
      int character = Serial.read();
      switch (character) {
        case 0x74: // "t"
          counter = 31;
          Serial.println();
          setTime();
          while (Serial.available() > 0) Serial.read(); // discard other data
        break;
        
        case 0x71: // "q"
          counter = 1;
          Serial.println();
          while (Serial.available() > 0) Serial.read(); // discard other data
        break;
        
        default: 
          Serial.println("??");
        break;
      }
    }
  }
  Serial.println();
}

void printLog(){
  File dataFile = SD.open("datalog.txt");
  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();
  }
  // if the file can't open, pop up an error:
  else {
    Serial.println(F("Can't find \"datalog.txt\""));
  }
}

void deleteLog(){
  if (SD.remove("datalog.txt")) {
    Serial.println(F("Deleted \"datalog.txt\""));
  } else {
    Serial.println(F("Can't find \"datalog.txt\""));
  }
}

void setTime(){
/*
  currentDateTime = RTC.now();
  Serial.print(F("Now: "));
  if (currentDateTime.day() < 10) Serial.print("0");
  Serial.print(currentDateTime.day());
  Serial.print('/');
  if (currentDateTime.month() < 10) Serial.print("0");
  Serial.print(currentDateTime.month());
  Serial.print('/');
  if (currentDateTime.year() < 10) Serial.print("0");
  Serial.print(currentDateTime.year());
  Serial.print(" - ");
  if (currentDateTime.hour() < 10) Serial.print("0");
  Serial.print(currentDateTime.hour());
  Serial.print(':');
  if (currentDateTime.minute() < 10) Serial.print("0");
  Serial.print(currentDateTime.minute());
  Serial.print(':');
  if (currentDateTime.second() < 10) Serial.print("0");
  Serial.print(currentDateTime.second());
  Serial.println();
/**/  
  Serial.print(F("YY: "));
  Serial.setTimeout(30000);
  byte year = Serial.parseInt();
  Serial.println(year);

  Serial.print(F("MM: "));
  byte month = Serial.parseInt();
  Serial.println(month);

  Serial.print(F("DD: "));
  byte date = Serial.parseInt();
  Serial.println(date);

  Serial.print(F("hh: "));
  byte hour = Serial.parseInt();
  Serial.println(hour);

  Serial.print(F("mm: "));
  byte minute = Serial.parseInt();
  Serial.println(minute);

  Serial.print(F("ss: "));
  byte second = Serial.parseInt();
  Serial.println(second);

  Clock.setYear(year);
  Clock.setMonth(month);
  Clock.setDate(date);
  Clock.setHour(hour);
  Clock.setMinute(minute);
  Clock.setSecond(second);

  delay(100);
  currentDateTime = RTC.now();
  Serial.print(F("New time: "));
  if (currentDateTime.day() < 10) Serial.print("0");
  Serial.print(currentDateTime.day());
  Serial.print('/');
  if (currentDateTime.month() < 10) Serial.print("0");
  Serial.print(currentDateTime.month());
  Serial.print('/');
  if (currentDateTime.year() < 10) Serial.print("0");
  Serial.print(currentDateTime.year());
  Serial.print(" - ");
  if (currentDateTime.hour() < 10) Serial.print("0");
  Serial.print(currentDateTime.hour());
  Serial.print(':');
  if (currentDateTime.minute() < 10) Serial.print("0");
  Serial.print(currentDateTime.minute());
  Serial.print(':');
  if (currentDateTime.second() < 10) Serial.print("0");
  Serial.print(currentDateTime.second());
  Serial.println();
}
/*
void setInterval(){
  Serial.print(F("\nCurrently: "));
  Serial.print(sleepMinutes);
  Serial.println(F(" min."));
  Serial.print(F("Set to: "));
  Serial.setTimeout(30000);
  sleepMinutes = Serial.parseInt();
  Serial.print(sleepMinutes);
  Serial.println(" min.");
  EEPROM.update(EEPROM_ADDR_SLEEP_MINUTES, sleepMinutes);
}
*/

/**************************
 * MEASURE and LOG stuff: *
 **************************/

void logError(__FlashStringHelper *errmessage) {
  Serial.print(F("[ERR] "));
  Serial.println(errmessage);
}


void logMeasurements() {
  logString = DEVICE_NAME;

  addDateTime();

#if (DEVICE_TYPE == AIR_TYPE)
  addSoilMoisture();
#endif

#if (DEVICE_TYPE == H2O_TYPE)
  addWaterConductivity();
#endif
  
#if (DEVICE_TYPE == AIR_TYPE || DEVICE_TYPE == H2O_TYPE)
  addDS18B20();
#endif

#if DEVICE_TYPE == AIR_TYPE
  addWindCounts();
  addWindDir();
  addTSL2591();
#endif
  
#if (DEVICE_TYPE == AIR_TYPE)
  addBME280();
#endif

  addBATT();

  writeSD();
  Serial.println(logString);
  delay(1000);

#if DEVICE_TYPE == CAM_TYPE
  myCAMSaveToSDFile(myCAM1);
#endif

  if (battLow == true) {
    delay(5000);
    digitalWrite(PIN_PWR_MAIN, LOW);
  }


}

void addDateTime() {
  currentDateTime = RTC.now();
  logString += String('\t');
  if (currentDateTime.day() < 10) logString += String("0");
  logString += String(currentDateTime.day());
  logString += String('/');
  if (currentDateTime.month() < 10) logString += String("0");
  logString += String(currentDateTime.month());
  logString += String('/');
  if (currentDateTime.year() < 10) logString += String("0");
  logString += String(currentDateTime.year());
  logString += String('\t');
  if (currentDateTime.hour() < 10) logString += String("0");
  logString += String(currentDateTime.hour());
  logString += String(':');
  if (currentDateTime.minute() < 10) logString += String("0");
  logString += String(currentDateTime.minute());
  logString += String(':');
  if (currentDateTime.second() < 10) logString += String("0");
  logString += String(currentDateTime.second());
}

void addBATT() {
//  analogReference(DEFAULT); analogRead(PIN_BATT50); delay(10);
//  logString += String('\t');
//  logString += String(analogRead(PIN_BATT50)/155.53);
  analogReference(INTERNAL); analogRead(PIN_BATT25); delay(10);
  logString += String('\t');
  logString += String(analogRead(PIN_BATT25)/229.2);

#ifndef DISABLE_AUTO_OFF
  if ( (analogRead(PIN_BATT25)/229.2) < 3.4 ) {
    logError(F("LOW BATT"));
    logString += String('!');
    delay(1000);
    battLow = true;
  }
#endif
}

#if (DEVICE_TYPE == AIR_TYPE)
void addSoilMoisture() {
  analogReference(DEFAULT); analogRead(PIN_ADC); delay(10);
  logString += String('\t');
  logString += String(airCapZero-analogRead(PIN_ADC)/(airCapSlope/10.0));
}
#endif

#if (DEVICE_TYPE == H2O_TYPE)
void addWaterConductivity() {
  analogReference(DEFAULT); analogRead(PIN_ADC); delay(10);
  logString += String('\t');
  logString += String((analogRead(PIN_ADC)*(h2oCondSlope/10.0) +h2oCondZero));
}
#endif

#if DEVICE_TYPE == AIR_TYPE
void addWindDir() {
  analogReference(DEFAULT); analogRead(PIN_WIND_DIR); delay(10);
  logString += String('\t');
  int direction_analog = analogRead(PIN_WIND_DIR);
  if      (direction_analog < 138) logString += String("  0");
  else if (direction_analog < 236) logString += String(" 45");
  else if (direction_analog < 377) logString += String(" 90");
  else if (direction_analog < 551) logString += String("315");
  else if (direction_analog < 715) logString += String("135");
  else if (direction_analog < 844) logString += String("270");
  else if (direction_analog < 925) logString += String("225");
  else if (direction_analog >=925) logString += String("180");
}
#endif

#if (DEVICE_TYPE == AIR_TYPE || DEVICE_TYPE == H2O_TYPE)
void addDS18B20() {
  sensors.requestTemperatures(); // Send the command to get temperatures
  logString += String('\t');
  logString += String(sensors.getTempCByIndex(0));  
}
#endif

#if DEVICE_TYPE == AIR_TYPE
void addWindCounts() {
  while ((millis() - windStartTime) < 1999) {delay(1);}
  logString += String('\t');
  logString += String(windCounter*0.6);
  //Serial.println(millis()-windStartTime);
}
#endif

#if DEVICE_TYPE == AIR_TYPE
void addTSL2591() {
  uint16_t ir, full;
  uint32_t lum;

  tsl.setGain(TSL2591_GAIN_MED);      // 25x gain
  lum = tsl.getFullLuminosity();
  ir = lum >> 16;
  full = lum & 0xFFFF;
  logString += String('\t');
  logString += String(tsl.calculateLux(full, ir),1);

  tsl.setGain(TSL2591_GAIN_MAX);      // 9876x gain
  lum = tsl.getFullLuminosity();
  ir = lum >> 16;
  full = lum & 0xFFFF;
  logString += String('\t');
  logString += String(tsl.calculateLux(full, ir)*1000,0);
}

#endif

void addBME280(){
#if (DEVICE_TYPE == AIR_TYPE)
  uint8_t counter = 0;
  if (bme280int.measure()) {
    while ( (!bme280int.hasValue()) && (counter <= 20) ) {delay(100); counter++;}
  } else {
    logError(F("BMEi"));
  }
  if (counter >= 20) logError(F("BMEi"));

  logString += String('\t');
  logString += String(bme280int.getHumidity());
  logString += String('\t');
  logString += String(bme280int.getTemperature());
  logString += String('\t');
  logString += String(bme280int.getPressure()/100.0);
#endif

#if DEVICE_TYPE == AIR_TYPE
  if (bme280ext.measure()) {
    while ( (!bme280ext.hasValue()) && (counter <= 20) ) {delay(100); counter++;}
  } else {
    logError(F("BMEx"));
  }
  if (counter >= 20) logError(F("BMEx"));

  logString += String('\t');
  logString += String(bme280ext.getHumidity());
  logString += String('\t');
  logString += String(bme280ext.getTemperature());
  logString += String('\t');
  logString += String(bme280ext.getPressure()/100.0);
#endif
}

void writeSD() {
  dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile) {
    dataFile.println(logString);
    dataFile.close();
  } else {
    logError(F("SDw"));
  }
}

/*********************************
 * POWER DOWN, SLEEP/WAKE stuff: *
 *********************************/

void shutDown() {
  SD.end();
  turnOffSDcard();
  delay(100);
  digitalWrite(PIN_PWR_PERI, LOW);
  pinMode(PIN_MOSI,INPUT);
  pinMode(PIN_CS_CAM,INPUT); digitalWrite(PIN_CS_CAM,HIGH);
  pinMode(PIN_CS_SD,INPUT);  digitalWrite(PIN_CS_SD,HIGH);
  pinMode(PIN_SCK,INPUT);
  pinMode(PIN_SCL,INPUT);
  pinMode(PIN_SDA,INPUT);
}

void sleep() {
  detachInterrupt(digitalPinToInterrupt(PIN_WIND_SPEED));
  for (int seconds=0; seconds < sleepMinutes*60; seconds=seconds+10){
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
    digitalWrite(PIN_LED, HIGH);
    LowPower.powerDown(SLEEP_15MS, ADC_OFF, BOD_OFF);
    digitalWrite(PIN_LED, LOW);
  }
  EIFR |= (1 << INTF0);    // clear any outstanding interrupt 0
#if DEVICE_TYPE == AIR_TYPE
  attachInterrupt(digitalPinToInterrupt(PIN_WIND_SPEED), countWind, CHANGE);
  windStartTime = millis();
  windCounter = 0;
#endif
}
void wakeUp() {
  digitalWrite(PIN_PWR_PERI, HIGH); // Turn on peripherals power
  delay(200);
  turnOnSDcard();
  initSD();

  initI2C();

#if DEVICE_TYPE == AIR_TYPE
  initTSL2591();
#endif

  initBME280();

#if DEVICE_TYPE == CAM_TYPE
  initCamera();
#endif
}

void turnOnSDcard() {
  //pinMode(SDpowerPin, OUTPUT); digitalWrite(SDpowerPin, HIGH); //turn on the BJT on SD ground line
  delay(6);                                            // let the card settle

  // some cards will fail on power-up unless SS is pulled up  ( &  D0/MISO as well? )
  DDRB = DDRB | (1<<DDB5) | (1<<DDB3) | (1<<DDB2); // set SCLK(D13), MOSI(D11) & SS(D10) as OUTPUT  
  // Note: | is an OR operation so  the other pins stay as they were.                (MISO stays as INPUT) 
  PORTB = PORTB & ~(1<<DDB5);  // disable pin 13 SCLK pull-up – leave pull-up in place on the other 3 lines
  
  power_spi_enable();                      // enable the SPI clock 
  SPCR=keep_SPCR;                          // enable SPI peripheral
  delay(10);  //SDcardOn = true;       // just a flag
}

void turnOffSDcard() {
#if DEVICE_TYPE == CAM_TYPE
  delay(5000);
#endif
  delay(6);
  SPCR = 0;                                         // disable SPI
  power_spi_disable();                     // disable SPI clock

  DDRB &= ~((1<<DDB5) | (1<<DDB4) | (1<<DDB3) | (1<<DDB2));   // set All SPI pins to INPUT
//  PORTB |= ((1<<DDB5) | (1<<DDB4) | (1<<DDB3) | (1<<DDB2));     // set ALL SPI pins HIGH (~30k pullup)
  // Note: you must disconnect the LED on pin 13 or you’ll bleed current through the limit resistor
  
  LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF); // wait 1 sec for internal SDcard housekeeping 
  
  delay(6);
  //pinMode(SDpowerPin, OUTPUT); digitalWrite(SDpowerPin, LOW);  //turn off BJT controlling the ground line
  delay(6); //SDcardOn = false;
} 




/***************
 * INIT stuff: *
 ***************/

void initPower() {
  digitalWrite(PIN_PWR_MAIN, HIGH); // Keep power on. (While user is pressing the "ON" button.)
  pinMode(PIN_PWR_MAIN, OUTPUT);

  digitalWrite(PIN_PWR_PERI, HIGH); // Keep power on. (While user is pressing the "ON" button.)
  pinMode(PIN_PWR_PERI, OUTPUT);
}

void initLED(){
  pinMode(PIN_LED, OUTPUT);  
  digitalWrite(PIN_LED, HIGH); 
}
void initSD() {
  if (!SD.begin(PIN_CS_SD)) logError(F("SD"));
}


void initI2C() {
  Wire.begin();
}


void initSerial() {
  Serial.begin(9600);
}

#if DEVICE_TYPE == CAM_TYPE
void initCamera() {
  //Reset the CPLD
  myCAM1.write_reg(0x07, 0x80);
  delay(100);
  myCAM1.write_reg(0x07, 0x00);
  delay(1000);

  myCAM1.set_format(JPEG);
  myCAM1.InitCAM();
  myCAM1.write_reg(ARDUCHIP_TIM, VSYNC_LEVEL_MASK);   //VSYNC is active HIGH

  myCAM1.clear_fifo_flag();
  myCAM1.write_reg(ARDUCHIP_FRAMES, FRAMES_NUM);

  myCAM1.OV5642_set_JPEG_size(OV5642_2592x1944);delay(1000);
  //myCAM1.OV5642_set_Compress_quality(low_quality);delay(1000);
  //myCAM1.OV5642_set_Compress_quality(default_quality);delay(1000);
  myCAM1.OV5642_set_Compress_quality(high_quality);delay(1000);
  myCAM1.OV5642_set_Light_Mode(Manual_A);delay(1000);
  myCAM1.OV5642_set_Special_effects(BW);delay(1000);


  delay(1000);
  myCAM1.clear_fifo_flag();

}
#endif

void logHeader() {
#if DEVICE_TYPE == AIR_TYPE
  logString = F("\t  date  \t  time  \tSoil%\tTs\tkm/h\tdir\tLux\tmLux\tHi\tTi\tPi\tHx\tTx\tPx\tVbatt");
#endif

#if DEVICE_TYPE == H2O_TYPE
  logString = F("\t  date  \t  time  \tuS/cm\tTw\tVbatt");
#endif

Serial.println(logString);
  delay(1000);
  writeSD();
  delay(1000);
}
  
void logReboot() {
  logString = "";
  logString += DEVICE_NAME;
  logString += " v. ";
  logString += VERSION_STRING;
  logString += " build ";
  logString += __DATE__;
  logString += " boot at: \t";
  currentDateTime = RTC.now();
  logString += String(currentDateTime.day());
  logString += String('/');
  logString += String(currentDateTime.month());
  logString += String('/');
  logString += String(currentDateTime.year());
  logString += String('\t');
  logString += String(currentDateTime.hour());
  logString += String(':');
  logString += String(currentDateTime.minute());
  logString += String(':');
  logString += String(currentDateTime.second());
  Serial.println(logString);
  delay(1000);
  writeSD();
  delay(1000);
}

#if DEVICE_TYPE == AIR_TYPE
void initTSL2591() {
  if (tsl.begin()) {
    tsl.setGain(TSL2591_GAIN_MAX);                // Highest gain
    tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS); // Longest integration
  }
  else logError(F("Lux"));
}
#endif

#if (DEVICE_TYPE == AIR_TYPE || DEVICE_TYPE == H2O_TYPE)
void initDS18B20() {
  sensors.begin();
  sensors.setResolution(12);
}
#endif


void initBME280() {
#if (DEVICE_TYPE == AIR_TYPE)
  if (bme280int.begin()) {
    bme280int.resetToDefaults();
    bme280int.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
    bme280int.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);
    bme280int.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);
  } else {
    logError(F("BMEi"));
  }
#endif

#if DEVICE_TYPE == AIR_TYPE
  if (bme280ext.begin()) {
    bme280ext.resetToDefaults();
    bme280ext.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
    bme280ext.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);
    bme280ext.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);
  } else {
    logError(F("BMEx"));
  }
#endif
}


#if DEVICE_TYPE == AIR_TYPE
void initWindSpeed() {
  pinMode(PIN_WIND_SPEED, INPUT_PULLUP);
  EIFR |= (1 << INTF0);    // clear any outstanding interrupt 0
  attachInterrupt(digitalPinToInterrupt(PIN_WIND_SPEED), countWind, CHANGE);
  windStartTime = millis();
  windCounter = 0;
}
#endif

#if DEVICE_TYPE == AIR_TYPE
void countWind() {
  windCounter++;
}
#endif

#if DEVICE_TYPE == CAM_TYPE
void myCAMSaveToSDFile(ArduCAM myCAM){
char str[13];
char date[8];
byte buf[256];
static int i = 0;
static int k = 0;
uint8_t temp = 0,temp_last=0;
uint32_t length = 0;
bool is_header = false;
File outFile;
//Flush the FIFO
myCAM.flush_fifo();
//Clear the capture done flag
myCAM.clear_fifo_flag();
//Start capture
digitalWrite(PIN_PWR_FLASH, HIGH);
delay(100);
myCAM.start_capture();
//Serial.println(F("start Capture"));
while(!myCAM.get_bit(ARDUCHIP_TRIG , CAP_DONE_MASK));
digitalWrite(PIN_PWR_FLASH, LOW);
//Serial.println(F("Capture Done."));  
length = myCAM.read_fifo_length();
//Serial.print(F("The fifo length is :"));
//Serial.println(length, DEC);
if (length >= MAX_FIFO_SIZE) //8M
{
  Serial.println(F("Over size."));
  return ;
}
if (length == 0 ) //0 kb
{
  Serial.println(F("Size is 0."));
  return ;
}
currentDateTime = RTC.now();
//Construct a file name
k = k + 1;
sprintf(str, "%02d%02d%02d%02d.jpg",currentDateTime.month(),currentDateTime.day(),currentDateTime.hour(),currentDateTime.minute());
//Serial.println(str);
//itoa(currentDateTime.month(), str, 10);
//itoa(currentDateTime.day(), date, 10);
//strcat(str, date);
//itoa(currentDateTime.hour(), date, 10);
//strcat(str, date);
//itoa(currentDateTime.minute(), date, 10);
//strcat(str, date);
//strcat(str, ".jpg");
//Open the new file
outFile = SD.open(str, O_WRITE | O_CREAT | O_TRUNC);
if(!outFile){
  Serial.println(F("File open failed"));
  return;
}
myCAM.CS_LOW();
myCAM.set_fifo_burst();
while ( length-- )
{
  temp_last = temp;
  temp =  SPI.transfer(0x00);
  //Read JPEG data from FIFO
  if ( (temp == 0xD9) && (temp_last == 0xFF) ) //If find the end ,break while,
  {
    buf[i++] = temp;  //save the last  0XD9     
    //Write the remain bytes in the buffer
    myCAM.CS_HIGH();
    outFile.write(buf, i);    
    //Close the file
    outFile.close();
    Serial.println(str);
//    Serial.println(F(" Image save OK."));
    is_header = false;
    i = 0;
  }  
  if (is_header == true)
  { 
    //Write image data to buffer if not full
    if (i < 256)
      buf[i++] = temp;
    else
    {
      //Write 256 bytes image data to file
      myCAM.CS_HIGH();
      outFile.write(buf, 256);
      i = 0;
      buf[i++] = temp;
      myCAM.CS_LOW();
      myCAM.set_fifo_burst();
    }        
  }
  else if ((temp == 0xD8) & (temp_last == 0xFF))
  {
    is_header = true;
    buf[i++] = temp_last;
    buf[i++] = temp;   
  } 
} 
}
#endif
