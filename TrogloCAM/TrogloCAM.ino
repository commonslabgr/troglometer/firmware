/* TrogloCAM.ino
 * 
 * Firmware for a data-logging environment monitor "Troglometer", specifically for the CAM module.
 * 
 * Copyright (c) 2020 by Jann Eike KRUSE and CommonsLab Koin.S.Ep.
 * 
 * This program is Free Software, since it is 
 * licensed under the GNU AGPL 3.0 licence. 
 * 
 */

#define D 0
#define VERSION "TrogloCAM V3.0g"
#define AUTO_SHUTDOWN true

enum DigitalPin {
  PIN_RX          =  0, // Serial UART receive data  (from user to ARDUINO)
  PIN_TX          =  1, // Serial UART transmit data (from ARDUINO to user)
  PIN_WIND_SPEED  =  2, // Interrupt'able pin for anemometer clicks.
  PIN_SQW         =  3, // Interrupt'able pin for clock alarm (or "square wave")
  PIN_DS          =  4, // Dallas One-Wire bus
  PIN_LED         =  5, // Status LED
  PIN_PWR_FLASH   =  6, // Power up the IR flash light
  PIN_PWR_PERI    =  7, // Power up the peripherals (sensors, SD reader, clock, etc.)
  PIN_PWR_MAIN    =  8, // Power up main power bus (flash, peripherals and ARDUINO power)
  PIN_CS_SD       =  9, // SPI: Chip-select signal for the SD-card reader
  PIN_CS_CAM      = 10, // SPI: Chip-select signal for the ArduCAM
  PIN_MOSI        = 11, // SPI: Master-Out--Slave-In
  PIN_MISO        = 12, // SPI: Master-In--Slave-Out
  PIN_SCK         = 13, // SPI: Serial Clock
};

enum AnalogPin {
  PIN_ADC      = A0,
  PIN_WIND_DIR = A1,
//PIN_A2       = A2,
//PIN_A3       = A3,
  PIN_SDA      = A4,
  PIN_SCL      = A5,
  PIN_BATT25   = A6,
  PIN_BATT50   = A7,
};

byte  keep_SPCR;

#include "DS3231.h"
#include <Wire.h>
DS3231 Clock;
RTClib RTC;

#include <SPI.h>
#include <SD.h>
#include "LowPower.h"
#include <avr/power.h>
File myFile;

#include <ArduCAM.h>
#include "memorysaver.h"
//This can only work on OV5642_MINI_5MP_PLUS platform.
#if !(defined (OV5642_MINI_5MP_PLUS))
#error Please select the hardware platform and camera module in the ../libraries/ArduCAM/memorysaver.h file
#endif
ArduCAM CAM(OV5642, PIN_CS_CAM);

uint16_t sleepMinutes = 10; // Number of minutes to stay asleep.


void setup() {
  // Here we do NOT put the setup that has to happen after sleep.
  // We put only the setup that happens ONCE upon START / POWER-ON.

  pinMode(PIN_PWR_MAIN, OUTPUT);
  digitalWrite(PIN_PWR_MAIN, HIGH);    // turn the main power on
  
  pinMode(PIN_PWR_FLASH, OUTPUT);
  digitalWrite(PIN_PWR_FLASH, LOW);    // turn the flash off
  
  pinMode(PIN_CS_CAM, OUTPUT);
  digitalWrite(PIN_CS_CAM, HIGH);      // Set CAM CS to idle.
  
  SPI.begin();    // SPI.begin() is necessary for the CAM but can be called only once!
  keep_SPCR=SPCR; // So we need to save the default SPCR register contents for later.

  Serial.begin(9600);
  Serial.println(VERSION);
  pinMode(PIN_LED, OUTPUT);
  analogWrite(PIN_LED, 1);      // Dimly light the LED
}

void loop() {
  delay(1);
  turnOnSDcardMy();
  delay(1);
  initCAM();
  delay(1);
  initSD();
  delay(1);
  if (millis()<20000) logBoot();
  delay(100);
  SDlogV();
  delay(1);
  SDreadInterval();
  delay(1);
  if (millis()<20000) bootMessage();
  useCAM();
  delay(1);
  shutDown();
  delay(1);
  sleep(sleepMinutes);
}

void logBoot() {
  if(D)Serial.print("logBoot..");
    myFile = SD.open("DATALOG.TXT", FILE_WRITE);
  if (myFile) {
    myFile.println(VERSION);
    myFile.close();
    if(D)Serial.println(".done.");
  } else {
    Serial.println("ERROR opening DATALOG.TXT for writing");
  }
}

void bootMessage() {
  while (Serial.available() > 0) Serial.read(); // discard previous data
  if(D)Serial.println( F("Press [ENTER].") );
  for (uint8_t counter = 10; counter >= 1; counter--) {
    Serial.print(counter);
    Serial.print( F(".") );
    digitalWrite(PIN_LED, HIGH); 
    delay(100);
    digitalWrite(PIN_LED, LOW); 
    delay(900);
    while (Serial.available() > 0) {
      int character = Serial.read();
      switch (character) {
        case 10:
        case 13: 
          counter = 1;
          bootMenu(); 
        break;
          
        default: 
          Serial.write(character); 
          Serial.println("??");
        break;
      }
    }
  }
  Serial.println();
}

void bootMenu() {
  while (Serial.available() > 0) Serial.read(); // discard previous data
  Serial.println( F("\nt: Set time\nq: Quit menu") );
  for (uint8_t counter = 30; counter >= 1; counter--) {
    Serial.print(counter);
    Serial.print( F("..") );
    digitalWrite(PIN_LED, HIGH); 
    delay(50);
    digitalWrite(PIN_LED, LOW); 
    delay(100);
    digitalWrite(PIN_LED, HIGH); 
    delay(50);
    digitalWrite(PIN_LED, LOW); 
    delay(800);
    while (Serial.available() > 0) {
      int character = Serial.read();
      switch (character) {
        case 0x74: // "t"
          counter = 31;
          Serial.println();
          setTime();
          while (Serial.available() > 0) Serial.read(); // discard other data
        break;
        
        case 0x71: // "q"
          counter = 1;
          Serial.println();
          while (Serial.available() > 0) Serial.read(); // discard other data
        break;
        
        default: 
          Serial.println("??");
        break;
      }
    }
  }
  Serial.println();
}

void setTime(){
  DateTime currentDateTime = RTC.now();
  Serial.print(F("Now: "));
  if (currentDateTime.day() < 10) Serial.print("0");
  Serial.print(currentDateTime.day());
  Serial.print('/');
  if (currentDateTime.month() < 10) Serial.print("0");
  Serial.print(currentDateTime.month());
  Serial.print('/');
  if (currentDateTime.year() < 10) Serial.print("0");
  Serial.print(currentDateTime.year());
  Serial.print(" - ");
  if (currentDateTime.hour() < 10) Serial.print("0");
  Serial.print(currentDateTime.hour());
  Serial.print(':');
  if (currentDateTime.minute() < 10) Serial.print("0");
  Serial.print(currentDateTime.minute());
  Serial.print(':');
  if (currentDateTime.second() < 10) Serial.print("0");
  Serial.print(currentDateTime.second());
  Serial.println();

  Serial.print(F("YY: "));
  Serial.setTimeout(30000);
  byte year = Serial.parseInt();
  Serial.println(year);

  Serial.print(F("MM: "));
  byte month = Serial.parseInt();
  Serial.println(month);

  Serial.print(F("DD: "));
  byte date = Serial.parseInt();
  Serial.println(date);

  Serial.print(F("hh: "));
  byte hour = Serial.parseInt();
  Serial.println(hour);

  Serial.print(F("mm: "));
  byte minute = Serial.parseInt();
  Serial.println(minute);

  Serial.print(F("ss: "));
  byte second = Serial.parseInt();
  Serial.println(second);

  Clock.setYear(year);
  Clock.setMonth(month);
  Clock.setDate(date);
  Clock.setHour(hour);
  Clock.setMinute(minute);
  Clock.setSecond(second);

  delay(100);
  currentDateTime = RTC.now();
  Serial.print(F("New time: "));
  if (currentDateTime.day() < 10) Serial.print("0");
  Serial.print(currentDateTime.day());
  Serial.print('/');
  if (currentDateTime.month() < 10) Serial.print("0");
  Serial.print(currentDateTime.month());
  Serial.print('/');
  if (currentDateTime.year() < 10) Serial.print("0");
  Serial.print(currentDateTime.year());
  Serial.print(" - ");
  if (currentDateTime.hour() < 10) Serial.print("0");
  Serial.print(currentDateTime.hour());
  Serial.print(':');
  if (currentDateTime.minute() < 10) Serial.print("0");
  Serial.print(currentDateTime.minute());
  Serial.print(':');
  if (currentDateTime.second() < 10) Serial.print("0");
  Serial.print(currentDateTime.second());
  Serial.println();
}

/* FIRST-LEVEL functions called from loop(): */

void SDreadInterval() {
  if(D)Serial.print("SDreadInterval..");
  char str[6];
  uint8_t stringPointer = 0;
  File dataFile = SD.open("interval.txt");

  if (dataFile) {
    while ((dataFile.available() && stringPointer < 5)) {
      str[stringPointer]=dataFile.read();
      stringPointer++;
    }
    dataFile.close();
    sleepMinutes = atol(str);
    //EEPROM.update(EEPROM_ADDR_SLEEP_MINUTES, sleepMinutes);
  if(D)Serial.print(sleepMinutes);
  if(D)Serial.println(".DONE.");
  }
}

void useCAM() {
  if(D)Serial.print("testCAM..");
  CAM.set_format(JPEG);
  CAM.OV5642_set_Compress_quality(low_quality);
  CAM.InitCAM();
  CAM.write_reg(ARDUCHIP_TIM, VSYNC_LEVEL_MASK);   //VSYNC is active HIGH
  CAM.clear_fifo_flag();
  CAM.write_reg(ARDUCHIP_FRAMES, 0);
  delay(1000);
  //CAM.OV5642_set_JPEG_size(OV5642_320x240); delay(1000);
  CAM.OV5642_set_JPEG_size(OV5642_2592x1944); delay(1000);
  CAM.OV5642_set_Compress_quality(high_quality);delay(1000);
  CAM.OV5642_set_Light_Mode(Manual_A);delay(1000);
  CAM.OV5642_set_Special_effects(BW);delay(1000);
  CAM.clear_fifo_flag();

  myCAMSaveToSDFile(CAM);

  pinMode(PIN_CS_CAM, OUTPUT);
  digitalWrite(PIN_CS_CAM, HIGH);      // Set CAM CS to idle.

  if(D)Serial.println(".DONE.");
}

void wakeUp() {
  Serial.print("wakeUp..");
  turnOnSDcardMy();
  Serial.println(".DONE.");
}

void initCAM() {
  if(D)Serial.print("initCAM..");
  //Reset the CPLD
  CAM.write_reg(0x07, 0x80);
  delay(100);   
  CAM.write_reg(0x07, 0x00);
  CAM.write_reg(ARDUCHIP_TEST1, 0x55);
  if(CAM.read_reg(ARDUCHIP_TEST1) == 0x55)
  {
    if(D)Serial.print("SPI OK.");
  } else {
    Serial.print("SPI Error!");
  }
  if(D)Serial.println(".DONE.");
}


void initSD() {
  if ( SD.begin(PIN_CS_SD) == true ) {
    if(D)Serial.println("SD init done.");
  } else {
    Serial.println("SD init FAILED!!!");
  }
}

void SDlogV() {
  if(D)Serial.print("SDlogV..");
  myFile = SD.open("DATALOG.TXT", FILE_WRITE);
  if (myFile) {
    analogReference(INTERNAL); analogRead(PIN_BATT25); delay(10);
    DateTime currentDateTime = RTC.now();
    if(D)Serial.print("Writing to DATALOG.txt...");
    myFile.print("CAM\t");
    if (currentDateTime.day() < 10) myFile.print("0");
    myFile.print(currentDateTime.day());
    myFile.print("/");
    if (currentDateTime.month() < 10) myFile.print("0");
    myFile.print(currentDateTime.month());
    myFile.print("/");
    if (currentDateTime.year() < 10) myFile.print("0");
    myFile.print(currentDateTime.year());
    myFile.print("\t");
    if (currentDateTime.hour() < 10) myFile.print("0");
    myFile.print(currentDateTime.hour());
    myFile.print(":");
    if (currentDateTime.minute() < 10) myFile.print("0");
    myFile.print(currentDateTime.minute());
    myFile.print(":");
    if (currentDateTime.second() < 10) myFile.print("0");
    myFile.print(currentDateTime.second());
    myFile.print("\t");
    myFile.print(analogRead(PIN_BATT25)/229.2);
    myFile.println("");

    myFile.close();
    if(D)Serial.println(".done.");
  } else {
    Serial.println("ERROR opening DATALOG.TXT for writing");
  }
}

void shutDown() {
  if(D)Serial.println("shutingDown.\n\n\n");
  Serial.flush();
  SD.end();
  turnOffSDcardMy();
  SPI.end();
  delay(100);
  digitalWrite(PIN_PWR_PERI, LOW);

  if((AUTO_SHUTDOWN) && (analogRead(PIN_BATT25)/229.2 < 3.3) ) digitalWrite(PIN_PWR_MAIN, LOW);

  pinMode(PIN_MOSI,INPUT);
  digitalWrite(PIN_MOSI, LOW);    // Disable MOSI

  pinMode(PIN_MISO,INPUT);
  digitalWrite(PIN_MISO, LOW);    // Disable MISO
  
  pinMode(PIN_SCK,INPUT);
  digitalWrite(PIN_SCK, LOW);     // Disable Clock  
  
  pinMode(PIN_CS_CAM,INPUT_PULLUP);
  pinMode(PIN_CS_SD,INPUT_PULLUP);

  pinMode(PIN_SCL,INPUT);
  pinMode(PIN_SDA,INPUT);
}


void sleep(int sleepMinutes) {
  Serial.flush();             // Wait for all UART data to be transmitted.
  digitalWrite(PIN_LED, LOW);

  for (int seconds=0; seconds < sleepMinutes*60; seconds=seconds+10){
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
    digitalWrite(PIN_LED, HIGH);
    delay(2);
    digitalWrite(PIN_LED, LOW);
  }
  analogWrite(PIN_LED, 5);    // turn the status led dimly on
}


void turnOffSDcardMy() {
  if(D)Serial.print("OffSD..");
  delay(6);
  SPCR = 0;                                         // disable SPI
  power_spi_disable();                     // disable SPI clock
  
  DDRB &= ~((1<<DDB5) | (1<<DDB4) | (1<<DDB3) | (1<<DDB2));   // set All SPI pins to INPUT
  PORTB |= ((1<<DDB5) | (1<<DDB4) | (1<<DDB3) | (1<<DDB2));     // set ALL SPI pins HIGH (~30k pullup)
  // Note: you must disconnect the LED on pin 13 or you’ll bleed current through the limit resistor
  
  //LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF); // wait 1 sec for internal SDcard housekeeping 
  
  delay(6);
  pinMode(PIN_PWR_PERI, OUTPUT); digitalWrite(PIN_PWR_PERI, LOW);  //turn off BJT controlling the ground line
  delay(6);
  if(D)Serial.println(".DONE.");
}


void turnOnSDcardMy() {
  if(D)Serial.print("OnSD..");
  pinMode(PIN_PWR_PERI, OUTPUT); digitalWrite(PIN_PWR_PERI, HIGH);
  delay(6);                                            // let the card settle

  // some cards will fail on power-up unless SS is pulled up  ( &  D0/MISO as well? )
  DDRB = DDRB | (1<<DDB5) | (1<<DDB3) | (1<<DDB2); // set SCLK(D13), MOSI(D11) & SS(D10) as OUTPUT  
  // Note: | is an OR operation so  the other pins stay as they were.                (MISO stays as INPUT) 
  PORTB = PORTB & ~(1<<DDB5);  // disable pin 13 SCLK pull-up – leave pull-up in place on the other 3 lines
  
  power_spi_enable();                      // enable the SPI clock 
  SPCR=keep_SPCR;                          // enable SPI peripheral
  delay(10);
  if(D)Serial.println(".DONE.");
}

void myCAMSaveToSDFile(ArduCAM myCAM){
  char str[8];
  byte buf[256];
  static int i = 0;
  static int k = 0;
  uint8_t temp = 0,temp_last=0;
  uint32_t length = 0;
  bool is_header = false;
  File outFile;
  //Flush the FIFO
  myCAM.flush_fifo();
  //Clear the capture done flag
  myCAM.clear_fifo_flag();
  //Start capture
  digitalWrite(PIN_PWR_FLASH,HIGH);
  delay(50);
  myCAM.start_capture();
  if(D)Serial.println(F("start Capture"));
  uint16_t countdown = 1000;
  while( (!myCAM.get_bit(ARDUCHIP_TRIG , CAP_DONE_MASK)) && (countdown > 0)){
    countdown--;
    delay(1);
  }
  digitalWrite(PIN_PWR_FLASH,LOW);
  if (countdown == 0) {
    myFile = SD.open("DATALOG.TXT", FILE_WRITE);
    if (myFile) {
      myFile.println("Capture failed.");
      myFile.close();
    } else {
      Serial.println("ERROR opening DATALOG.TXT for writing");
    }
  
  }
  if(D)Serial.print(F("Capture Done at countdown = "));  
  if(D)Serial.println(countdown);  
  digitalWrite(PIN_PWR_FLASH,LOW);
  length = myCAM.read_fifo_length();
  if(D)Serial.print(F("The fifo length is :"));
  if(D)Serial.println(length, DEC);
  if (length >= MAX_FIFO_SIZE) //8M
  {
    Serial.println(F("Over size."));
    return ;
  }
  if (length == 0 ) //0 kb
  {
    Serial.println(F("Size is 0."));
    return ;
  }
  //Construct a file name
  DateTime currentDateTime = RTC.now();
  sprintf(str, "%02d%02d%02d%02d.jpg",currentDateTime.month(),currentDateTime.day(),currentDateTime.hour(),currentDateTime.minute());
  Serial.print(str);
  //Open the new file
  outFile = SD.open(str, O_WRITE | O_CREAT | O_TRUNC);
  if(!outFile){
    Serial.println(F(" File open faild"));
    return;
  }
  myCAM.CS_LOW();
  myCAM.set_fifo_burst();
  while ( length-- )
  {
    temp_last = temp;
    temp =  SPI.transfer(0x00);  
    if(is_header == true) 
    if(is_header == false)   analogWrite(PIN_LED,length);

    //Read JPEG data from FIFO
    if ( (temp == 0xD9) && (temp_last == 0xFF) ) //If find the end
    {
      buf[i++] = temp;  //save the last  0XD9     
      //Write the remain bytes in the buffer
      myCAM.CS_HIGH();
      outFile.write(buf, i);    
      outFile.close();
      Serial.println(F("Image saved."));
      is_header = false;
      i = 0;
    }  
    if (is_header == true)
    { 
      //Write image data to buffer if not full
      if (i < 256)
        buf[i++] = temp;
      else
      {
        //Write 256 bytes image data to file
        myCAM.CS_HIGH();
        outFile.write(buf, 256);
        digitalWrite(PIN_LED,HIGH);
        i = 0;
        buf[i++] = temp;
        myCAM.CS_LOW();
        myCAM.set_fifo_burst();
        digitalWrite(PIN_LED,LOW);
      }        
    }
    else if ((temp == 0xD8) & (temp_last == 0xFF))
    {
      is_header = true;
      buf[i++] = temp_last;
      buf[i++] = temp;   
      analogWrite(PIN_LED, 1);    // turn the status led dimly on
    } 
  } 
}
